

# Cancel Vs Match Volume Graph

## Installation

Requires Python 3.

```pip install -r requirements.txt```

## Run

```python mvc.py```

## About

A tool from greenthumble. This shows orderbook cancelations compared with actual trades for XBTUSD.

If you found this tool useful please consider making a donation: 1K9tzAgTs86kdC3fXJQ1MwFhYkgW1RvWYZ
