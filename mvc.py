
from pyqtgraph.Qt import QtGui, QtCore
from time import time
from math import log, floor, ceil
import pyqtgraph as pg
import numpy as np
import threading
import traceback
import websocket
import urllib
import json
import webbrowser
import pyimgur
import os

IMGUR_CLIENT_ID = '00bb34ac658660c'
SCRENSHOT_FILENAME = 'mvctmpupload.png'

class MvCMainWindow(QtGui.QMainWindow):
 
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Match vs Cancel')
        mainMenu = self.menuBar() 
        fileMenu = mainMenu.addMenu('File')
        helpMenu = mainMenu.addMenu('Help')
        exitButton = QtGui.QAction(QtGui.QIcon('exit24.png'), 'Exit', self)
        exitButton.setShortcut('Ctrl+Q')
        exitButton.setStatusTip('Exit application')
        exitButton.triggered.connect(self.close)
        uploadButton = QtGui.QAction('Upload screenshot', self)
        uploadButton.setStatusTip('Create screenshot and upload to imgur.com')
        uploadButton.triggered.connect(self.upload)
        uploadButton.setShortcut('Ctrl+P')
        fileMenu.addAction(uploadButton)
        fileMenu.addAction(exitButton)
        self.show()

    def upload(self):
        pix=view.grab()
        pix.save(SCRENSHOT_FILENAME)
        im = pyimgur.Imgur(IMGUR_CLIENT_ID)
        uploaded_image = im.upload_image(SCRENSHOT_FILENAME, title='MvC Screenshot')
        os.unlink(SCRENSHOT_FILENAME)
        webbrowser.open(uploaded_image.link)

tstart = floor(time()) - 1

app = QtGui.QApplication([])
mw = MvCMainWindow()
mw.resize(800,800)
view = pg.GraphicsLayoutWidget()
mw.setCentralWidget(view)
mw.show()
w = view.addPlot(name="mainchart")
v = view.addPlot(name="volumeprofile")
gray = brush=pg.mkBrush(255, 255, 255, 100)
red = brush=pg.mkBrush(255, 0, 0, 100)
green = brush=pg.mkBrush(0, 255, 0, 100)
blue = brush=pg.mkBrush(0, 0, 255, 100)
yellow = brush=pg.mkBrush(255, 255, 0, 100)
cvm = pg.ScatterPlotItem(size=10, pen=pg.mkPen(None), brush=gray)
#vwap = pg.PlotDataItem(size=1, pen=pg.mkPen(None), brush=blue, connect='all')
price = pg.PlotDataItem(size=1, pen={'color': "FFF", 'width': 1}, connect='all')
#histw = pg.BarGraphItem(x=[], pen={'color': "660", 'width': 1}, width=[], height=10)
histw = pg.PlotDataItem(x=[], pen={'color': "660", 'width': 1}, width=[], height=10)
#w.enableAutoRange(axis=pg.ViewBox.XAxis, enable=True)
#w.enableAutoRange(axis=pg.ViewBox.YAxis, enable=True)
v.setFixedWidth(200)
v.invertX()
w.setAutoPan(True, False)
#w.setMouseEnabled(x=False, y=True)
v.setMouseEnabled(x=False, y=True)

#histw.setLogMode(True, False)
v.showAxis('bottom', False)
v.showAxis('left', False)
v.showAxis('right', True)
w.showAxis('left', False)
v.setYLink(w)
w.addItem(cvm)
#w.addItem(vwap)
w.addItem(price)
v.addItem(histw)
v.hideButtons()

def convertsize(n):
    return log(n) * 2.5

# This is a hacked up bitmex-ws.
class MvCBitMEXWebsocket:

    MAX_TABLE_LEN = 200
    MAX_GRAPH_LEN = 5000

    def __init__(self, symbol):
        url = self.__get_url("https://www.bitmex.com/api/v1", symbol)
        print('Connecting to : %s' % (url,))
        self.ws = websocket.WebSocketApp(url, on_message=self.__on_message,)
        self.wst = threading.Thread(target=lambda: self.ws.run_forever())
        self.wst.daemon = True
        self.wst.start()
        self.data = {}
        self.keys = {}
        self.graph = []
        self.price = []

    def __on_message(self, ws, message):
        '''Handler for parsing WS messages.'''
        message = json.loads(message)

        table = message['table'] if 'table' in message else None
        action = message['action'] if 'action' in message else None
        try:
            if action:

                if table not in self.data:
                    self.data[table] = []

                # There are four possible actions from the WS:
                # 'partial' - full table image
                # 'insert'  - new row
                # 'update'  - update row
                # 'delete'  - delete row
                if action == 'partial':
                    self.data[table] += message['data']
                    # Keys are communicated on partials to let you know how to uniquely identify
                    # an item. We use it for updates.
                    self.keys[table] = message['keys']
                elif action == 'insert':
                    self.data[table] += message['data']

                    # Limit the max length of the table to avoid excessive memory usage.
                    # Don't trim orders because we'll lose valuable state if we do.
                    if table not in ['order', 'orderBookL2'] and len(self.data[table]) > MvCBitMEXWebsocket.MAX_TABLE_LEN:
                        self.data[table] = self.data[table][int(MvCBitMEXWebsocket.MAX_TABLE_LEN / 2):]
                    graph = []
                    if table == 'trade':
                        for data in message['data']:
                            price = data['price']
                            size = data['size']
                            side = data['side']
                            t = floor(time()) - tstart
                            brush = red if side == 'Sell' else green
                            graph.append({'pos': (t, price), 'size': convertsize(size), 'brush': brush})
                            if len(self.price):
                                color = 'F00' if self.price[-1]['y'] > price else '0F0'
                            else: color = 'FFF'
                            self.price.append({'x': t, 'y': price, 'color': color})
                            #self.price.append((t, price))
                        self.add_points(graph)
                        self.updatevwap()

                elif action == 'update':
                    # Locate the item in the collection and update it.
                    for updateData in message['data']:
                        item = findItemByKeys(self.keys[table], self.data[table], updateData)
                        if not item:
                            return  # No item found to update. Could happen before push
                        item.update(updateData)
                        # Remove cancelled / filled orders
                        if table == 'order' and item['leavesQty'] <= 0:
                            self.data[table].remove(item)
                elif action == 'delete':
                    # Locate the item in the collection and remove it.
                    t = floor(time()) - tstart
                    graph = []
                    prices = []
                    sizes = []
                    for deleteData in message['data']:
                        item = findItemByKeys(self.keys[table], self.data[table], deleteData)
                        prices.append(item['price'])
                        sizes.append(item['size'])
                        #price = item['price']
                        #size = item['size']
                        self.data[table].remove(item)
                        #graph.append({'pos': (t, price), 'size': convertsize(size), 'brush': gray})
                    if len(sizes) > 0:
                        price = sum(prices) / float(len(prices))
                        size = sum(sizes)
                        graph.append({'pos': (t, price), 'size': convertsize(size), 'brush': gray})
                        if 'trade' in self.data and len(self.data['trade']) > 0:
                            lastp = self.data['trade'][-1]['price']
                            bottom = lastp / 10.0
                            if price > bottom - 50 and price < bottom + 50:
                                graph.append({'pos': (t, price*10.0), 'size': convertsize(size*10.0), 'brush': blue})
                            top = lastp * 10.0
                            if price > top - 5000 and price < top + 5000:
                                graph.append({'pos': (t, price/10.0), 'size': convertsize(size*10.0), 'brush': yellow})
                    self.add_points(graph)
                else:
                    raise Exception("Unknown action: %s" % action)
        except:
            print(traceback.format_exc())

    def __get_url(self, url, symbol):
        symbolSubs = ["execution", "instrument", "order", "orderBookL2", "position", "quote", "trade"]
        genericSubs = ["margin"]
        subscriptions = [sub + ':' + symbol for sub in symbolSubs]
        subscriptions += genericSubs
        urlParts = list(urllib.parse.urlparse(url))
        urlParts[0] = urlParts[0].replace('http', 'ws')
        urlParts[2] = "/realtime?subscribe={}".format(','.join(subscriptions))
        return urllib.parse.urlunparse(urlParts)

    def add_points(self, graph):
        self.graph = self.graph + graph
        l = len(self.graph)
        if l > MvCBitMEXWebsocket.MAX_GRAPH_LEN:
            self.graph = self.graph[l-MvCBitMEXWebsocket.MAX_GRAPH_LEN:]
        l = len(self.price)
        if l > MvCBitMEXWebsocket.MAX_GRAPH_LEN:
            self.price = self.price[l-MvCBitMEXWebsocket.MAX_GRAPH_LEN:]

    def updatevwap(self):
        pass

def findItemByKeys(keys, table, matchData):
    for item in table:
        matched = True
        for key in keys:
            if item[key] != matchData[key]:
                matched = False
        if matched:
            return item

ws = MvCBitMEXWebsocket("XBTUSD")

timer = QtCore.QTimer()
def updatecvm():
    try:
        cvm.setPoints(ws.graph)
        price.setData(ws.price)
    except: pass
    if 'orderBookL2' in ws.data:
        bookhistogram = {}
        for order in ws.data['orderBookL2']:
            p = ceil(order['price'] / 10) * 10
            if p not in bookhistogram: bookhistogram[p] = 0
            bookhistogram[p] += order['size']
        x = []
        y = []
        for sk in sorted(bookhistogram.keys()):
            y.append(sk)
            x.append(bookhistogram[sk])
        x = np.array(x)
        y = np.array(y)
        #histw.setOpts(width=0.4+x*0.9, x=x*0.3+2, y=y)
        try: histw.setData(x=x, y=y)
        except: pass
    #w.plot(ws.price)
timer.setInterval(1500)
timer.timeout.connect(updatecvm)
timer.start()

if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
